//
//  Flashcard.swift
//  falshcards
//
//  Created by Jason Gallo on 1/19/22.
//

import Foundation

class cards{
    func cars()-> Dictionary<String, String>{
        var carComponents = ["Engine": "a machine with moving parts that converts power into motion.",
                        "Suspension": "the system of springs and shock absorbers by which a vehicle is cushioned from road conditions.",
                        "Fuel": "material such as coal, gas, or oil that is burned to produce heat or power.",
                        "Force Induction": "the process of delivering compressed air to the intake of an internal combustion engine.",
                        "Tires": "ring-shaped component that surrounds a wheel's rim to transfer a vehicle's load from the axle through the wheel to the ground and to provide traction on the surface over which the wheel travels.",
                        "Drivetrain": "the system in a motor vehicle which connects the transmission to the drive axles.",
                        "Transmission": "the mechanism by which power is transmitted from an engine to the wheels of a motor vehicle.",
                        "Cylinders": " piston chamber in a steam or internal combustion engine.",
                        "Displacement": "the volume swept by a reciprocating system, as in a pump or engine",
                        "Motor oil": "any one of various substances that consist of base oils enhanced with various additives, particularly antiwear additives, detergents, dispersants, and, for multi-grade oils, viscosity index improvers."]
    
    return carComponents;
    }
}
